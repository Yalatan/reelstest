
import { SpinnerProperties as SP } from "./spinnerProperties";
let SpinnerProperties = new SP();

enum State {
    Spinup,
    Spindown
}

export class Spinner {
    reel_speed: any;
    renderer: any;
    spinReelsState = false;
    reel_position:  any;
    reel_pixel_length: any;
    reels: any;
    reel_speed_change = 3;
    spinup_change = 2;
    spindown_change = 1;
    max_reel_speed = 32;
    start_slowing: any;
    stop_index: any;
    stopping_position: any;
    stopping_distance = 528;
    game_state: any;



    constructor(renderer: any) {
        this.renderer = renderer;
        let btn = document.getElementById("start");
        btn.addEventListener("click", (e: Event) => this.spin());

        this.reel_speed = new Array(SpinnerProperties.REEL_COUNT);
        for (var i = 0; i < SpinnerProperties.REEL_COUNT; i++) {
            this.reel_speed[i] = 0;

        }

        this.reels = new Array(SpinnerProperties.REEL_COUNT);
        this.reels[0] = new Array(12, 1, 7, 1, 2, 7, 6, 7, 3, 10, 1, 6, 1, 7, 3, 4, 3, 2, 4, 5, 0, 6, 10, 5, 6, 5, 8, 3, 0, 9, 5, 12);
        this.reels[1] = new Array(6, 0, 10, 3, 6, 7, 9, 2, 5, 2, 3, 1, 5, 2, 1, 10, 4, 5, 8, 4, 7, 6, 0, 1, 7, 6, 3, 1, 5, 9, 7, 4);
        this.reels[2] = new Array(1, 4, 2, 7, 5, 6, 4, 10, 7, 14, 2, 0, 6, 4, 10, 1, 7, 6, 3, 0, 5, 7, 2, 3, 9, 3, 5, 6, 1, 8, 1, 3);
        this.reels[3] = new Array(6, 0, 10, 3, 6, 7, 9, 2, 5, 2, 3, 1, 5, 2, 1, 13, 4, 5, 8, 4, 7, 6, 0, 1, 7, 6, 3, 1, 5, 9, 7, 4);
        this.reels[4] = new Array(1, 4, 2, 7, 5, 6, 4, 10, 7, 5, 2, 0, 6, 4, 10, 1, 7, 6, 3, 0, 5, 7, 2, 3, 9, 13, 5, 6, 1, 8, 1, 3);

        this.reel_position = new Array(SpinnerProperties.REEL_COUNT);
        for (var i = 0; i < SpinnerProperties.REEL_COUNT; i++) {
            this.reel_position[i] = Math.floor(Math.random() * SpinnerProperties.REEL_POSITIONS) * SpinnerProperties.SYMBOL_SIZE;
        }
        renderer.setReels (this.reels, this.reel_position);
        this.reel_pixel_length = SpinnerProperties.REEL_POSITIONS * SpinnerProperties.SYMBOL_SIZE;

        this.start_slowing = new Array(SpinnerProperties.REEL_COUNT);
        this.stopping_position = new Array(SpinnerProperties.REEL_COUNT);



    }

    spin() {
    this.game_state = State.Spinup;
        setInterval(() => {
            this.logic();
            this.renderer.render_reel();
        }, 50);

}

    logic() {
        if (this.game_state == State.Spinup) {
            this.logic_spinup();
        }
        else if (this.game_state == State.Spindown) {
            this.logic_spindown();
        }

    }

    move_reel(i: any) {
        this.reel_position[i] -= this.reel_speed[i];

            if (this.reel_position[i] < 0) {
            this.reel_position[i] += this.reel_pixel_length;
        }
    }

    logic_spinup() {
        for (var i = 0; i < SpinnerProperties.REEL_COUNT; i++) {

            this.move_reel(i);
            this.reel_speed[i] += this.spinup_change;

        }
        if (this.reel_speed[0] == this.max_reel_speed) {


            this.set_stops();
            this.game_state = State.Spindown;
        }

    }
    logic_spindown() {

        for (var i = 0; i < SpinnerProperties.REEL_COUNT; i++) {


            this.move_reel(i);
            if (this.start_slowing[i] == false) {

                var check_position = false;
                if (i == 0) check_position = true;
                else if (this.start_slowing[i - 1]) check_position = true;

                if (check_position) {

                    if (this.reel_position[i] == this.stopping_position[i]) {
                        this.start_slowing[i] = true;
                    }
                }
            }
            else {
                if (this.reel_speed[i] > 0) {
                    this.reel_speed[i] -= this.spindown_change;


                }
            }
        }
    }


    set_stops() {
        for (var i = 0; i < SpinnerProperties.REEL_COUNT; i++) {

            this.start_slowing[i] = false;

            this.stop_index = Math.floor(Math.random() * SpinnerProperties.REEL_POSITIONS);
            this.stopping_position[i] = this.stop_index * SpinnerProperties.SYMBOL_SIZE;

            this.stopping_position[i] += this.stopping_distance;
            if (this.stopping_position[i] >= this.reel_pixel_length) this.stopping_position[i] -= this.reel_pixel_length;

        }

    }


}


