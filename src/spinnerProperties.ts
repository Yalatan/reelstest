export class SpinnerProperties {
    public REEL_POSITIONS: number = 32;
    public SYMBOL_SIZE: number = 64;
    public REEL_COUNT: number = 5;
    public ROW_COUNT: number = 3;
}

