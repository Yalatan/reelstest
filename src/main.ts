import {Renderer} from "./renderer";
import {Spinner} from "./spinner";

var renderer;
var spinner;

window.onload = function () {
    let can = document.getElementById("myCan");
    renderer = new Renderer(can);
    spinner = new Spinner(renderer);

};
