import {SpinnerProperties as SP } from "./spinnerProperties";
let SpinnerProperties = new SP();

export class Renderer {
    can: any;
    ctx: any;
    symbols: any;
    slot_bg: any;
    reels: any;
    reel_position:  any;
    reel_area_left = 32;
    reel_area_top = 32;
    reel_area_width = 320;
    reel_area_height = 192;


    constructor(canvas: any) {
        this.can = canvas;
        this.ctx = canvas.getContext("2d");
        this.symbols = new Image();
        this.slot_bg = new Image();
        this.slot_bg.src = "./images/white_background.png";
        this.symbols.src = "./images/fruits.png";

        this.symbols.onload = () => {
            this.render_reel();
        };

    }

    render_reel() {
        var reel_index;
        var symbol_offset;
        var symbol_index;
        var x;
        var y;


        this.ctx.drawImage(this.slot_bg, 0, 0);

        this.ctx.beginPath();
        this.ctx.rect(this.reel_area_left, this.reel_area_top, this.reel_area_width, this.reel_area_height);
        this.ctx.clip();

        for (var i = 0; i < SpinnerProperties.REEL_COUNT; i++) {
            for (var j = 0; j < SpinnerProperties.ROW_COUNT + 1; j++) {

                reel_index = Math.floor(this.reel_position[i] / SpinnerProperties.SYMBOL_SIZE) + j;
                symbol_offset = this.reel_position[i] % SpinnerProperties.SYMBOL_SIZE;


                if (reel_index >= SpinnerProperties.REEL_POSITIONS) reel_index -= SpinnerProperties.REEL_POSITIONS;

                symbol_index = this.reels[i][reel_index];

                x = i * SpinnerProperties.SYMBOL_SIZE;
                y = j * SpinnerProperties.SYMBOL_SIZE - symbol_offset;

                this.draw_symbol(symbol_index, x, y);

            }
        }
    }

    draw_symbol(symbol_index: any, x: number, y: number) {
        var symbol_pixel = symbol_index * SpinnerProperties.SYMBOL_SIZE;
        this.ctx.drawImage(this.symbols, 0, symbol_pixel, SpinnerProperties.SYMBOL_SIZE, SpinnerProperties.SYMBOL_SIZE, x + this.reel_area_left, y + this.reel_area_top, SpinnerProperties.SYMBOL_SIZE, SpinnerProperties.SYMBOL_SIZE);

    }
    setReels (reels: any, reel_position: any) {
        this.reel_position = reel_position;
        this.reels = reels;

    }

}

